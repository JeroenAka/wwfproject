import sqlite3
from sqlite3 import Error

#CONSTANTS:
db_path = r"database\Mainframe.sqlite"

def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection


def init_db():
    con = create_connection(db_path)
    cur = con.cursor()

    cur.execute('SELECT name from sqlite_master where type="table"')

    res = cur.fetchall()
    print('Tables found: ', len(res), ' ', res)

    cur.execute('create table if not exists countries(ck integer, country text, country_code text)')

    con.commit()

    con.close()

def main():

    # Program flow:
    # Initialize database
    #   Check if required tables exist
    #       if not, create required tables
    #       fill tables with base data (countries) from csv
    # Load UI
    # Wait for user input

    init_db()



    pass


if __name__ == '__main__':
    main()
